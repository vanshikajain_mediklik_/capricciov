import QtQuick

Rectangle {
  property alias text: txt.text

  implicitWidth: txt.width + 12; implicitHeight: txt.height + 12
  radius: 6
  color: "gray"

  Text {
    id: txt
    x: 6; y: 6
    color: "white"
  }
}
