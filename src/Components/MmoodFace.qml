import QtQuick.Shapes

import com.mediklik.capriccio
import QtQuick

Rectangle {
  id: moodRect

  property alias moodType: mood.type

  width: Math.min(parent.width, parent.height) * 0.8; height: width
  radius: width / 2
  color: "yellow"

  Mmood { // C++ defined
    id: mood
  }

  Rectangle { // left eye
    id:leftEye
    x: moodRect.width * 0.25; y: moodRect.height * 0.25
    width: moodRect.width * 0.05; height: blinkState ? 0:width; radius: width / 2
    color: "white"
    Rectangle {
      x:0.25*parent.width
      y:0.25*parent.height
    // anchors.centerIn: parent.centerIn

    width: parent.width /2; height:parent.height*0.5 ; radius: width / 2
    color: "black"

  }
  }
   Rectangle { // right eye
     id:rightEye
    x: moodRect.width * 0.75; y: moodRect.height * 0.25//fix
    width: moodRect.width * 0.05; height: blinkState ? 0:width; radius: width / 2
    color: "white"
    Rectangle {
      x:0.25*parent.width
      y:0.25*parent.height
    // anchors.centerIn: parent.centerIn

    width: parent.width /2; height: parent.height*0.5; radius: width / 2
    color: "black"
    /*opacity: blinkState ? 0 : 1 */// Opacity controls blinking effect
  }
  }


  Shape {
    id: snoot
    anchors.fill: parent
    ShapePath {
      fillColor: "transparent"
      capStyle: ShapePath.RoundCap
      strokeColor: "#fff"
      strokeWidth: moodRect.width / 50
      startX: snoot.width / 2; startY: snoot.height * 0.4
      PathLine { relativeX: -snoot.width * 0.05; relativeY: snoot.height * 0.2 }
      PathLine { relativeX: snoot.width * 0.05; relativeY: -snoot.height * 0.05 }
    }
  }

 Shape {
    id: smile
    property real yFactor: height / (Mmood.MoodCount - 1)
    x: moodRect.width * 0.2
    y: moodRect.height - height - smile.yFactor * mood.type / 2.0
    width: moodRect.width * 0.6; height: width * 0.5


    ShapePath {
      id: smilePath
      fillColor: "transparent"
      capStyle: ShapePath.RoundCap
      strokeColor: "black"
      strokeWidth: moodRect.width / 50
      startX: 0; startY: smile.yFactor * mood.type
      PathQuad {
        x: smile.width; y: smile.yFactor * mood.type
        controlX: smile.width / 2
        controlY: smile.height - smile.yFactor * mood.type

      }
    }
    ShapePath { // mouth corners - only when very happy :-)


      fillColor: "transparent"
      capStyle: ShapePath.RoundCap
      strokeColor: mood.type === Mmood.MoodHapyHapy ? "black" : "transparent"
      strokeWidth: moodRect.width / 50
      PathMultiline {
        paths: [
          [ Qt.point(smile.yFactor / 2, -smile.yFactor / 2),
            Qt.point(-smile.yFactor / 2, smile.yFactor / 2) ],
          [ Qt.point(smile.width - smile.yFactor / 2, -smile.yFactor / 2),
            Qt.point(smile.width + smile.yFactor / 2, smile.yFactor / 2) ]
        ]

      }

    }
      // Animating the smile's y position
      Behavior on height {
            PropertyAnimation {
            id: smileAnimation
            target: smile
            property: "height"
            from: 150
            // to: 340 // Adjust this value according to your animation needs
            duration: 1000
             easing.type: Easing.InOutQuad
loops: Animation.Infinite
            }
        }




 }
  Timer {
    id: blinkTimer
    interval: 900 // Blink interval in milliseconds
    repeat: true
    running: true
    onTriggered: {
      blinkState = !blinkState
    }

  }
  property bool blinkState: false // Initial state of blinking

    // Function to trigger the smile animation
    function animateSmile() {
        smileAnimation.start();
    }

    // Signal handler to trigger the animation when mood type changes
    Connections {
        target: mood
        onTypeChanged: {
            animateSmile();
        }
    }

}
