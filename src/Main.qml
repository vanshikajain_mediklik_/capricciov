import QtQuick
import QtQuick.Window
import QtQuick.Controls

import "Components"

Window {
  id: moodWindow

  width: 1024; height: 768
  visible: true
  title: "Capriccio No. 1"

  MmoodFace {
    y: 10
    anchors.horizontalCenter: parent.horizontalCenter
    moodType: moodCombo.currentIndex
  }

  Rectangle {
    id: bottomRect
    anchors {
      horizontalCenter: parent.horizontalCenter
      bottom: parent.bottom
      bottomMargin: moodWindow.height * 0.01
    }
    width: moodWindow.width * 0.8; height: moodWindow.height * 0.1
    radius: height * 0.1
    color: "#e5e5e5"

    MframeLabel {
      x: bottomRect.width * 0.1
      anchors.verticalCenter: parent.verticalCenter
      text: moodCombo.currentText
    }
    ComboBox {
      id: moodCombo
      x: bottomRect.width * 0.9 - width
      anchors.verticalCenter: parent.verticalCenter
      width: 200
      model: [ "more than happy", "just happy", "balanced", "a bit sad", "broken" ]
      currentIndex: 0
    }
  }
}
